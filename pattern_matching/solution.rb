# str : remaining STRING
# pat : remaining PATTERN
# hash : current word map for checking
def is_matching(str, pat, hash)
  return true if str.length.zero? && pat.length.zero?
  return false if str.length.zero? || pat.length.zero?

  char = pat[0]
  # hash includes current char
  if hash.key? char 
    word = hash[char]
    return false if word.length > str.length || !str.start_with?(word)
    return is_matching(str[word.length..-1], pat[1..-1], hash)
  end

  # hash doesn't include current char, add char to hash and keep checking
  new_hash = hash.dup
  k = 1
  while k <= str.length
    new_hash.store(char, str[0..k-1])
    return true if is_matching(str[k..-1], pat[1..-1], new_hash)

    k += 1
  end

  return false
end

# CHECK str is matching with pattern word
def solution(str, pattern) 
  is_matching(str, pattern, {})
end


problems = [
  { pattern: 'abab', str: 'redblueredblue' },
  { pattern: 'aaaa', str: 'asdasdasdasd' },
  { pattern: 'aabb', str: 'xyzabcxzyabc' },
  { pattern: 'abccab', str: 'redblueacacredblue' },
]

problems.each do |p|
  str = p[:str]
  pattern = p[:pattern]
  puts "pattern: #{pattern} , str: #{str} => #{solution(str, pattern)}"
end
